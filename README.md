# Exercise8
Основы разработки графических интерфейсов GUI

Данные файлы тестировались в IntelliJ и Eclipse IDE

Чтобы работало в Eclipse Che ищу плагины или другие элементы, которые надо подключить для корректной работы

#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

# Запуск c пакетами ( успешно )

{
    "tasks": [
        {
            "type": "che",
            "label": "Document build and run",
            "command": "javac -d /projects/Lab8/TextEditor/bin -sourcepath /projects/Lab8/TextEditor/src /projects/Lab8/TextEditor/src/Editors/Document.java && java -classpath /projects/Lab8/TextEditor/bin Editors.Document",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab8/TextEditor/src",
                "component": "maven"
            }
        },
        {
            "type": "che",
            "label": "FileStreams build and run",
            "command": "javac -d /projects/Lab8/TextEditor/bin -sourcepath /projects/Lab8/TextEditor/src /projects/Lab8/TextEditor/src/Editors/FileStreams.java && java -classpath /projects/Lab8/TextEditor/bin Editors.FileStreams",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab8/TextEditor/src",
                "component": "maven"
            }
        },
         {
            "type": "che",
            "label": "Main build and run",
            "command": "javac -d /projects/Lab8/TextEditor/bin -sourcepath /projects/Lab8/TextEditor/src /projects/Lab8/TextEditor/src/Editors/Main.java && java -classpath /projects/Lab8/TextEditor/bin Editors.Main",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab8/TextEditor/src",
                "component": "maven"
            }
        },
        {
            "type": "che",
            "label": "TextAreaTest build and run",
            "command": "javac -d /projects/Lab8/TextEditor/bin -sourcepath /projects/Lab8/TextEditor/src /projects/Lab8/TextEditor/src/Editors/TextAreaTest.java && java -classpath /projects/Lab8/TextEditor/bin Editors.TextAreaTest",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab8/TextEditor/src",
                "component": "maven"
            }
        },
        {
            "type": "che",
            "label": "TextFieldTest build and run",
            "command": "javac -d /projects/Lab8/TextEditor/bin -sourcepath /projects/Lab8/TextEditor/src /projects/Lab8/TextEditor/src/Editors/TextFieldTest.java && java -classpath /projects/Lab8/TextEditor/bin Editors.TextFieldTest",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab8/TextEditor/src",
                "component": "maven"
            }
        }
    ]
}

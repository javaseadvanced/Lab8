package Editors;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class TextFieldTest extends JFrame
{
 // ��������� ����
 JTextField smallField, bigField;

 public TextFieldTest()
 {
     super("��������� ����");
     setDefaultCloseOperation(EXIT_ON_CLOSE);
     // �������� ��������� �����
     smallField = new JTextField(15);
     smallField.setToolTipText("�������� ����");
     bigField = new JTextField("����� ����", 25);
     bigField.setToolTipText("������ ����");
     // ��������� ������
     bigField.setFont(new Font("Dialog", Font.PLAIN, 14));
     bigField.setHorizontalAlignment(JTextField.RIGHT);
     // ��������� ��������� �����
     smallField.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
             // ����������� ���������� ������
             JOptionPane.showMessageDialog(TextFieldTest.this, 
                            "���� �����: " + smallField.getText());
         }
     });
     // ���� � �������
     JPasswordField password = new JPasswordField(12);
     password.setEchoChar('*');
     // �������� ������ � ���������� ������
     JPanel contents = new JPanel(new FlowLayout(FlowLayout.LEFT));
     contents.add(smallField);
     contents.add(bigField  );
     contents.add(password  );
     setContentPane(contents);
     // ���������� ������ ���� � ������� ��� �� �����
     setSize(400, 130);
     setVisible(true);
 }
 public static void main(String[] args) {
     new TextFieldTest();
 }
}
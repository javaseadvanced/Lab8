package Editors;

import java.awt.BorderLayout;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
/*
��������� ��������
*/
// ��� �����-������
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
//��� ������� ��������� ������:
import java.util.StringTokenizer;

class FileStreams
{
 public static void main(String args[])
 {
   FrameWindow  frame; // ������ ���� (������ ����� FrameWindow, ������� �� �������� �� ������ ������ Frame)
   frame =  new FrameWindow("Simplest Java Notepad"); // �������� ���� (������ ����� FrameWindow)
   frame.setVisible(true); // ... � ������� ��� �������
 }//main
}//FileStreams

//================================
class FrameWindow extends Frame implements ActionListener, WindowListener
{
 TextArea ta; // ��������� �������
 MenuBar mb;  // ������ ���� 
 
 Menu mFile;  //���� File (�������� ��� � ������������) 
 
 // ������� ���� ����� �������� � ������������
 MenuItem miOpen;
 MenuItem miSave;
 MenuItem miSaveAs;
 MenuItem miExit;
 
 String PolniiPut = "";// ������ ���� � �����
 byte buf[];
 
 // ==================
 // ������ ����������� ������ FrameWindow
 public FrameWindow(String szTitle)
 {
   super(szTitle);
   setSize(800, 600);
   
   mb = new MenuBar();
   mFile = new Menu("File");
   
   miOpen = new MenuItem("Open...");
   mFile.add(miOpen);
   
   miSave = new MenuItem("Save");
   mFile.add(miSave);
   
   miSaveAs = new MenuItem("Save As...");
   mFile.add(miSaveAs);
   
   mFile.add("-");
   
   miExit = new MenuItem("Exit");
   mFile.add(miExit);
   
   mb.add(mFile);
   
   // �������� ���������� �� ������ ����
   miOpen.addActionListener(this);
   miSave.addActionListener(this);
   miSaveAs.addActionListener(this);
   miExit.addActionListener(this);
   
   setMenuBar(mb);//������ ���� ��������� � ����
   this.addWindowListener(this); // ��������� ��������� �� ���� (����� ������� "�������")
   
   ta = new TextArea(10, 30); // ������ ��������� �������
   setLayout(new BorderLayout());// ��������� ��� ���� ��������� �� �������� �����
   add("Center", ta);            // ��������� ��������� ������� �� ������ ����  
  }//FrameWindow

 // ============================================
 // actionPerformed
 // ============================================
 public void actionPerformed(ActionEvent e)
 {
   if(e.getSource().equals(miOpen))
   {
     FileOpen();
   }
   
   else if(e.getSource().equals(miSave))
   {
     FileSave();
   }
   
   else if(e.getSource().equals(miSaveAs))
   {
     FileSaveAs();
   }
   
   else if(e.getSource().equals(miExit))
   {
     setVisible(false);
     System.exit(0);
   }
 }//actionPerformed
 

 public void windowClosing(WindowEvent e)
 {
   setVisible(false);
   System.exit(0);
 }
 
 public void windowOpened(WindowEvent e) {}
 public void windowClosed(WindowEvent e) {}
 public void windowIconified(WindowEvent e) {}
 public void windowDeiconified(WindowEvent e) {}
 public void windowActivated(WindowEvent e) {}
 public void windowDeactivated(WindowEvent e) {}
 
 // ============================================
 void FileOpen()
 {
   FileDialog      FD; // ���������� ���� (��� ������ ������)
   FD = new FileDialog(this, "Open file",FileDialog.LOAD);
   FD.show(); 
   // ��������� ������ ���� � ���������� (� ������� ����������� ���� �����)
   PolniiPut = FD.getDirectory()+FD.getFile();
   // ������� ������ ���� � ����� � ��������� ������ 
   setTitle("Simplest Java Notepad" + " - " +PolniiPut);
   
   // "���������" ������ ��� ������ ������ �� ������
   FileInputStream FIS = null;//��� ������ ������ �� ������
       
   try
   {
     FIS = new FileInputStream(PolniiPut); // "���������" ������ ������ FileInputStream
     //  c ������, ������ ���� � ������� �������� � ���������� PolniiPut
     buf = new byte[FIS.available()]; // ������� ������ ������ (�� ���������� ������ � ����� (��� ����������� �� �������� ������))
     FIS.read(buf);// ��������� ����������� ������ � ������� ������ buf 
   }
   catch (IOException ex)
   {
     System.out.println(ex.toString());
   }
   
   // ����, ������ buf ������ ������ ����������� ��� ����� ������
   // ������� �� ���������� ��������� �������
   ta.selectAll();
   // ������� �� ���������� ��������� ������� (� ������� 0 �� ������� ���������� ���������� �������) �� ������ ������
   ta.replaceRange("", 0, ta.getSelectionEnd());
   
   // ����������� ������ ������ (buf) � ������
   String szStr = new String(buf);
   
   // ���������� ������ ������� ������� �������� �� ����� ������ (\r\n) 
   // ����� "�������" ������ �� ���� ��������, �������� ������ ������ StringTokenizer    
   
   StringTokenizer st;
   st = new StringTokenizer(szStr, "\r\n");//�������� ������� ������...
   
   // ���� ����� ��������, ���� ��������� ����� "�����" - ������� (\r\n) 
   while(st.hasMoreElements())
   {
     szStr = new String((String)st.nextElement());// �������� ��������� ������� ����������� � ����������� ��� � ������
     ta.append(szStr + "\r\n");// ��������� ���������� ������ � ��������� �������
   }//end_while  

   // ���� ���������, �� ���������� ����� ������������ � ��������� ������� 
   try
   {
    FIS.close();// FileInputStream ��� ������ �� ����� 
   }
   catch (IOException ex)
   {
     System.out.println(ex.toString());
   }
 }//FileOpen  
 
 // ==============================
 void FileSaveAs()
 {
   FileDialog FD;
   FD = new FileDialog(this, "Save file as...",  FileDialog.SAVE);
   FD.show();

   if(FD.getDirectory() == null || FD.getFile() == null) 
     {
      System.out.println("No File Selected!!!");
     }
    else 
     {
      PolniiPut = FD.getDirectory()+FD.getFile(); // ������ ���� � �������� �����
      setTitle("Simplest Java Notepad" + " - " +PolniiPut);
      FileSave();
     }
 }//FileSaveAs  
 
 // ========================================
 void FileSave()
 {
   FileOutputStream OS = null;//FileOutputStream - ��� ������ ������ � �����
   
   String sz = ta.getText(); // �� ���������� ��������� ������� �������� � ������
   buf = sz.getBytes(); // ����������� ������ � ������ ������ 
   
   if(PolniiPut=="" || PolniiPut==null)
    {
     System.out.println("No File Selected!!!");	
    }
   else // ���� ������
    {
     try 
      {
       OS = new FileOutputStream(PolniiPut);
       OS.write(buf);
       OS.close();
      }//try
     catch (IOException ex)
      {
       System.out.println(ex.toString());
      }	
   }//end_else (���� ������) 
 }//FileSave  
}//FrameWindow